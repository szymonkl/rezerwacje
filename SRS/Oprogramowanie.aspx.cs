﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SRS
{
    public partial class WebForm11 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Session["Uprawnienia"].ToString()) < 2) Response.Redirect("start.aspx");
        }

        protected void btDodajProgram_Click(object sender, EventArgs e)
        {
            SqlCommand cmd;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SRSConnectionString"].ConnectionString);
            con.Open();
            cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.CommandText = "AddProgram";
            SqlParameter param = new SqlParameter("@nazwa", tbNazwaProgramu.Text);
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();
            con.Close();
            gwOprogramowanie.DataBind();
        }

        protected void gwOprogramowanie_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            if (e.Exception != null)
            {
                lblDeleteError.Text = "Nie można usunąć sprzętu który jest na wyposażeniu sali.";
                e.ExceptionHandled = true;
            }
        }
    }
}