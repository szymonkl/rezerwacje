﻿<%@ Page Title="" Language="C#" MasterPageFile="~/index.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SRS.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="start" runat="server">
    <section>
        <div class="row">
            <h3 class="col-md-offset-1">Strona logowania</h3>
        </div>
        <br/>
        <br/>
        <div class="form-group">
            <div class="row col-md-offset-5 col-md-2">
                <label for="tbLogin">Login:</label>
                <asp:TextBox ID="tbLogin" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="LoginRFV" ForeColor="Red" runat="server" ErrorMessage="Proszę podać login" ControlToValidate="tbLogin"></asp:RequiredFieldValidator>
            </div>
            <div class="row col-md-offset-5 col-md-2">
                <label for="tbHaslo">Hasło:</label>
                <asp:TextBox ID="tbHaslo" runat="server" MaxLength="50" CssClass="form-control" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="HasloRFV" ForeColor="Red" runat="server" ErrorMessage="Proszę podać hasło" ControlToValidate="tbLogin"></asp:RequiredFieldValidator>
            </div>
            <div class="row">
                <asp:Button ID="btZaloguj" CssClass="btn btn-primary col-md-offset-5 col-md-2" runat="server" Text="Zaloguj" OnClick="btZaloguj_Click" />
                <span class="tdRFV">
                    <asp:CustomValidator ID="LoginCV" ForeColor="Red" runat="server" ErrorMessage="Niepoprawna nazwa użytkownika lub hasło!" OnServerValidate="LoginCV_ServerValidate"></asp:CustomValidator></span>
            </div>
        </div>
        
    </section>
</asp:Content>
