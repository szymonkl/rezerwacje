﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SRS
{
    public partial class WebForm10 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Session["Uprawnienia"].ToString()) < 2) Response.Redirect("start.aspx");
        }

        protected void btDodajSprzet_Click(object sender, EventArgs e)
        {
            SqlCommand cmd;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SRSConnectionString"].ConnectionString);
            con.Open();
            cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.CommandText = "AddSprzet";
            SqlParameter param = new SqlParameter("@nazwa", tbNazwaSprzetu.Text);
            cmd.Parameters.Add(param);
            
            cmd.ExecuteNonQuery();
            con.Close();
            GWSprzet.DataBind();
        }

        protected void GWSprzet_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            if (e.Exception != null)
            {
                lblDeleteError.Text = "Nie można usunąć sprzętu który jest na wyposażeniu sali.";
                e.ExceptionHandled = true;
            }
        }
    }
}