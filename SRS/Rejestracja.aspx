﻿<%@ Page Title="" Language="C#" MasterPageFile="~/index.Master" AutoEventWireup="true" CodeBehind="Rejestracja.aspx.cs" Inherits="SRS.WebForm3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="start" runat="server">
    <section>
        <h3 class="col-md-offset-1">Rejestracja:</h3>
        <div class="form-group">
            <div id="daneLogowania" class="col-md-offset-3 col-md-3">
                <div >
                    <label for="tbLogin">
                        Login:</label>
                    <asp:TextBox ID="tbLogin" CssClass="form-control" runat="server" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="LoginRFV" ForeColor="Red" runat="server" ErrorMessage="Proszę podać login" ControlToValidate="tbLogin" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="LoginCV" ForeColor="Red" runat="server" ErrorMessage="Użytkownik już istnieje" ControlToValidate="tbLogin" Display="Dynamic" OnServerValidate="LoginCV_ServerValidate"></asp:CustomValidator>
                </div>
                <div>
                    <label for="tbHaslo">
                        Hasło:</label>
                <asp:TextBox ID="tbHaslo" runat="server" CssClass="form-control" MaxLength="50" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="HasloRFV" ForeColor="Red" runat="server" ErrorMessage="Proszę podać hasło" ControlToValidate="tbHaslo"></asp:RequiredFieldValidator>
                </div>
                  <asp:Button ID="btZaloguj" runat="server" CssClass="btn btn-primary" Text="Zarejestruj" OnClick="btZaloguj_Click" />
            </div>
            <div id="daneKontaktowe" class="col-md-3">
            <div>
                <label for="tbNazwisko">
                    Nazwisko:</label>
                <asp:TextBox ID="tbNazwisko" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="NazwiskoRFV" ForeColor="Red" runat="server" ErrorMessage="Proszę podać nazwisko" ControlToValidate="tbNazwisko"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label for="tbImie">
                    Imię:</label>
                <asp:TextBox ID="tbImie" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ImieRFV" ForeColor="Red" runat="server" ErrorMessage="Proszę podać imię" ControlToValidate="tbImie"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label for="tbKontakt">
                    Kontakt:</label>
                <asp:TextBox ID="tbKontakt" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="KontaktRFV" ForeColor="Red" runat="server" ErrorMessage="Proszę podać dane kontaktowe" ControlToValidate="tbKontakt"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label for="tbRola">
                    Rola:</label>
                <asp:TextBox ID="tbRola" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RolaRFV" ForeColor="Red" runat="server" ErrorMessage="Proszę podać pełniną funkcję" ControlToValidate="tbRola"></asp:RequiredFieldValidator>
            </div>
            </div>
          


        </div>
       

    </section>
</asp:Content>
