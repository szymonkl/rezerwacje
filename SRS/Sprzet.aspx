﻿<%@ Page Title="" Language="C#" MasterPageFile="~/index.Master" AutoEventWireup="true" CodeBehind="Sprzet.aspx.cs" Inherits="SRS.WebForm10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="start" runat="server">
    <section>
        <p class="pNaglowek">Sprzęt</p>
            <table class="login_table">
                <tr>
                    <td style="text-align:center">
                        <asp:GridView ID="GWSprzet"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Id_Sprzet" DataSourceID="SDSSprzet" ForeColor="#333333" GridLines="None" HorizontalAlign="Center" OnRowDeleted="GWSprzet_RowDeleted">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="Id_Sprzet" HeaderText="Id_Sprzet" InsertVisible="False" ReadOnly="True" SortExpression="Id_Sprzet" Visible="False" />
                                <asp:BoundField DataField="Nazwa" HeaderText="Nazwa" SortExpression="Nazwa" />
                                <asp:CommandField CancelText="Anuluj" DeleteText="Usuń" EditText="Zmień" InsertText="Wstaw" ShowDeleteButton="True" ShowEditButton="True" UpdateText="Zmień" />
                            </Columns>
                            <EditRowStyle BackColor="#2461BF" />
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EFF3FB" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SDSSprzet" runat="server" ConnectionString="<%$ ConnectionStrings:SRSConnectionString %>" DeleteCommand="DELETE FROM [Sprzet] WHERE [Id_Sprzet] = @Id_Sprzet" InsertCommand="INSERT INTO [Sprzet] ([Nazwa]) VALUES (@Nazwa)" SelectCommand="SELECT * FROM [Sprzet] ORDER BY [Nazwa]" UpdateCommand="UPDATE [Sprzet] SET [Nazwa] = @Nazwa WHERE [Id_Sprzet] = @Id_Sprzet">
                            <DeleteParameters>
                                <asp:Parameter Name="Id_Sprzet" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="Nazwa" Type="String" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Nazwa" Type="String" />
                                <asp:Parameter Name="Id_Sprzet" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:Label ID="lblDeleteError" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center">
                     Nazwa:<asp:TextBox ID="tbNazwaSprzetu" runat="server"> </asp:TextBox>
                     <asp:Button ID="btDodajSprzet" runat="server" Text="Dodaj" OnClick="btDodajSprzet_Click"/>
                     <br />
                     <asp:RequiredFieldValidator ID="rfvNazwa"  runat="server" ErrorMessage="Proszę podać nazwę" ControlToValidate="tbNazwaSprzetu"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
    </section>
</asp:Content>
